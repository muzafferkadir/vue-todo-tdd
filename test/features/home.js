it('Should find empty list and add some datas.', () => {
    cy.visit('/');
  
    cy.get('[data-qa="todos"]').should('contain', 'Empty');

    cy.get('[data-qa="add-todo"]').clear().type('Buy tomato');

    cy.get('button').click()

    cy.get('[data-qa="todos"]').should('contain', 'Buy tomato');

    cy.get('[data-qa="add-todo"]').clear().type('Buy eggs');

    cy.get('button').click()

    cy.get('[data-qa="todos"]').should('contain', 'Buy eggs');
  });