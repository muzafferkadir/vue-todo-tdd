# vue-acceptance-test

## Project setup
```
npm install
```

```
 NOTE: You should create an env file and add `VUE_APP_BASE_URL` to test
```

### Compiles and hot-reloads for development
```
npm run serve
```
### Run tests
```
npm run test
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
